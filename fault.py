# -*- coding: utf-8 -*-
'''
@file fault.py

@brief This file protects the user from mechanical harm by utilizing overcuurent protection
@details This file utilizes the DRV8847 built in overccurrent prtection circuit and interupts to
protect against current spikes, such as when the motor is "stuck" and pinched agsint and object, such
as the user's finger. When an overcurrent is protected, and interupt is triggered, disabling the nSLEEP
pin and thus both motors. A second interupt is used to reset the fault when the user button is pushed.

@author: Cole and Ethan
'''

import pyb

class nFAULT:
    '''
    A class used to protect against overcurrent and disable motors. 
    '''
    
    def __init__ (self, nSLEEP_pin):
        '''
        Creates an nFAULT class given a pre-defined nSLEEP pin.
        '''
        
        # save variables to object:
        self.nSLEEP_pin = nSLEEP_pin
        
        # reset value
        self.fault = False
        
    def enable_fault(self):
        #Fault detection
        self.extint = pyb.ExtInt(pyb.Pin(pyb.Pin.cpu.B2, mode = pyb.Pin.IN),              #Setting pin nFAULT_pin to activate an EXTERNAL INTERRUPT
        pyb.ExtInt.IRQ_FALLING,     #Interrupt occurs on falling edge of signal
        pyb.Pin.PULL_NONE,            #Pull up resister is being used for this pin
        self.fault_callback)             #Define what interrupt service routine to go to when activated (fault_callback)
        
        # user button detection
        self.reset = pyb.ExtInt(pyb.Pin.cpu.C13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, self.reset)
        
    def fault_callback(self, pin):
        '''
        nFAULT function that is called by self.extint when overcurrent is trggered.
        '''
        print('nFAULT')
        self.extint.disable()
        pyb.delay(100)
        self.nSLEEP_pin.low()
        self.fault = True
        print('Push user button to reset fault!')
    
    def reset(self, pin):
        '''
        Reset function used to enable motors, True statement is used to detect if a fault has occured.
        '''
        
        if self.fault == True:
            self.nSLEEP_pin.high()
            self.fault = False
            print('Fault reset!')
            pyb.delay(100)
            self.extint.enable()
            
        else:
            pass