## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#  
#  @section sec_intro Introduction
#  This is the documentation website for Ethan Miller for a ME 400 project created and advised by Charlie Refvem.
#  The project consists of modelling a 5-member mechanism similar to one the one seen in Figure 1 with two motors. 
#  The goal of the project is to create a controller for the two motors that takes an input of the coordinate of the 
#  end condition and outputs angle values for the motors to be at. 
#  \n \image html five-bar-mechanism.png "Figure 1: Example of five-bar mechanism for a robotic dog leg"
#
#  
#  @subsection sec_pages Project Steps:
#     \ref diagram_of_sys
#  \n \ref kinematic_model
#  \n \ref motor_encoder_driver
#  \n \ref control_system
#  \n \n \n 
#  <CENTER>
#  @author Ethan Miller
#  @date June 6, 2021
#  @copyright License Info </CENTER>


#------------------------------------------------

##  @page diagram_of_sys Diagram of System
#   This projects goal was originally going to be used to model the end condition of the 5-member dog leg system,
#   but to simplify the modelling, only four of the members were modelled. Due to the last member being static, it would
#   be easy to implement the last condition to add that to the system. Figure 2 shows the diagram of the current system with
#   the angles that are being calculated. The end goal of the MATLAB code attached is to get the x and y coodinated of point 5
#   in terms of theta_1 and theta_2 which are the angles of the axles connected to the motors. Due to the complexity of
#   kinematic equations, this was done using curve fits with inputs coming from numerical estimations by the equations.
#   \n \image html Schematic.jpg "Figure 2: Schematic for four-bar mechanism for a robotic dog leg"  
#   
#  <CENTER> \n \n \n 
#  @author Ethan Miller
#  @date February 19, 2021
#  @copyright License Info </CENTER>

#------------------------------------------------

##  @page kinematic_model Kinematic Modelling
#   Using MATLAB, I was able to model the system in order to find a algebraic equation for translating the input of
#   the coordinates of the end condition to the get the needed output to control the system effectively which is the
#   theta values for the motors. This is needed because the only measuring device on the system is encoders attached 
#   to the motors. Most of the process is encompassed in the MATLAB code below through the comments.
#   
#   @subsection subsec_matlab MATLAB Code
#   <iframe src="DogLegEOMCalculations.html" width="100%" height=1000 ></iframe>
#  
#   @subsection subsec_source Source Code
#   You can see the source code at:
#   %%%% ADD URL %%%%%
# 
#   
#  <CENTER> \n \n \n 
#  @author Ethan Miller
#  @date February 19, 2021
#  @copyright License Info </CENTER>

#------------------------------------------------

##  @page motor_encoder_driver Motor and Encoder Drivers
#   This section outlines the creating generic MotorDriver and Encoder classes that can be created to send duty cycles to the motor and 
#   find the angle that the encoder is at. Figure 3 shows the physical motor and encoder system with the motor seen on the right and 
#   the encoder on the right. The two shafts are connected via a pully system that is running a 4:1 ratio, meaning the speed of the encoder
#   is 1/4 the speed of the motor.
#   \n \image html motors.jpg "Figure 3: Motor system that is driving axles." width=300px height=300px
#   
#   \n \n
#   A motor driver class encapsulates all of the features associated with driving a DC motor connected to the DRV8847 motor 
#   driver that is part of the base unit that we’ve connected the Nucleo to.  The base unit carries the DRV8847 motor driver 
#  from Texas Instruments which can be used to drive two DC motors or one stepper motor.
#  \n \n 
#  A common way to measure the movement of a motor is with an optical encoder.  Because most encoders can send out signals at high 
#  frequency (sometimes up to 1MHz or so),  we can’t readone with regular code; we need something faster, such as interrupts or 
#  dedicated hardware.  The STM32 processors have such hardware – timers which can read pulses from encoders and countdistance and 
#  direction of motion.  Incremental encoders send digital signals over two wires, usu-ally in quadrature, as shown below; 
#  the two signals are about 90 degrees out of phase.
#  \n \n
#  Another consideration that needed to be made was the fact that the motors were being underpowered by the driver chip, meaning it could
#  potenially pull more current than the board was capable of. This is why the \ref fault.py was created, which will shut down the motors so 
#  a circuit burnout doesn't occur. \n
#
#  @subsection subsec_doc Documentation
#  Please see \ref MotorDriver.py for documentation on the Motor Driver class. <br>
#  See \ref Encoder.py for documentation on the Encoder class.
#  See \ref fault.py for documentation on the fault class.
#
#  @subsection subsec_source Source Code
#  You can see the source code for the MotorDriver class at:
#  %%% ADD URL %%% <br>
#  and source code for the Encoder class at:
#  %%% ADD URL %%% <br>
#   
#  <CENTER> \n \n \n 
#  @author Ethan Miller
#  @date February 19, 2021
#  @copyright License Info </CENTER>

#------------------------------------------------

##  @page control_system Control System
#  To create the control system that will perform the preliminary tests for whether the kinematic equations would translate to an physical
#  system, a task based approach was taken. A simple three-state machine was created with an initialization state, data collection and 
#  calculation state, and finally motor signalling state. 
#  \n \n
#  In order to create the physical system, the four members were modeled in SolidWorks and 3D-printed and attached to the motor 
#  shafts to create the final system in Figure 4. The equations calculated in the \ref kinematic_model MATLAB script was transfered 
#  to python script manually and the ellipse that was found to be inside the bounds was converted into x and y coordinated that the 
#  device could follow.
#
#  \n \image html printed_system.jpg "Figure 4: 3D printed four-member system connected to two motor/encoder systems seen in Figure 3." width=300px height=300px
#
#  Documentation for this control system can be seen in the \ref main.py file. \n
#  You can see the source code for the main.py at:
#  https://bitbucket.org/emiller10/dog_leg_project/src/master/main.py <br>
#  
#  @subsection conc_controls Conclusions
#  Although I wasn't able to get the system working to the best of its ability due to hardware and time constraints,
#  I feel like this project served its purpose to verify the ability of a kinematic based controller to control a
#  complex system like this. 
#  
#  <CENTER> \n \n \n 
#  @author Ethan Miller
#  @date February 19, 2021
#  @copyright License Info </CENTER>