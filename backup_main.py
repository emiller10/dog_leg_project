# -*- coding: utf-8 -*-
"""
@file main.py


@author: Cole
"""

import pyb
import utime
import array
import numpy as np

class platformController:
    '''
    an FS
    '''
    
    ## Init state
    S0_INIT = 0
    ## State 1: updating positions and speed
    S1_Values = 1
    ## State 2: motor control
    S2_Motors = 2
    
    def __init__ (self, mot_th1, mot_th2, enc_th1, enc_th2, fault, K_th1, K_th2):
        '''
        creates a controller to balance the platform.
        @param k is a list representing gain values
        '''
        
        ## defining motors and controllers
        self.mot_x = mot_x
        self.mot_y = mot_y
        self.enc_x = enc_x
        self.enc_y = enc_y
        
        ## defining fault object for overpowering fault
        self.fault = fault
        
        ## defining gain values
        self.K_th1 = K_th1
        self.K_th2 = K_th2
        
        ## init positions and speed for motor x
        self.theta_1 = 0
        # self.thetad_1 = 0
        
        self.theta_2 = 0
        # self.thetad_2 = 0
        
        ## init path positions for system to follow
        total_time = 10 #seconds
        self.period = 100 #update time in Hz
        self.w = 2*pi()/total_time
        self.time = np.linspace(0,total_time,num=total_time*period,endpoint=True)
        
        # Center and radii of the ellipse path
        r_1 = 2.5
        r_2 = .75
        x_c = 2.75
        y_c = -8
        
        # Ellipse path in x and y parameterized by time
        self.x =  r_1*cos(self.w*self.time) + x_c;
        self.y = r_2*sin(self.w*self.time) + y_c;
        
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
       
        
        ###################################################
        
        ## Do you want to collect data?
        # self.data = False
        
        ####################################################
        
        
        #the timestamp since the first iteration
        self.start_time = utime.ticks_us() #the number of microseconds since hardware power on
        
        self.next_time = utime.ticks_add(self.start_time, int(self.period*1e6))       
        
    def run(self):
        '''
        runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()
        
        if(utime.ticks_diff(self.curr_time, self.next_time) >= 0):
        
            if(self.state == self.S0_INIT):
                ##
                self.enc_th1.set_position(3*pi()/2)

                
                self.enc_th2.set_position(3*pi()/2)
                self.transitionTo(self.S1_Values)
                
                self.mot_x.enable()
                self.mot_y.enable()
                
                self.fault.enable_fault()
            
            elif(self.state == self.S1_Values):
                
                self.enc_th1.update()
                self.enc_th2.update()
                
                #############################################################
                
                ## angle and angular velocity of platform
                self.theta_1 = self.enc_th1.get_position()
                # self.thetad_y = self.enc_x.get_delta()*0.001571*0.06/(0.110*self.period)
                
                self.theta_2 = self.enc_th2.get_position()
                #self.thetad_x = self.enc_y.get_delta()*0.001571*0.06/(0.110*self.period)
                
                #print(self.thetad_x)   
                #print(self.thetad_y)
                
                #############################################################
                
                
                

                #############################################################
                
                # print(self.contact)
                #print(self.x)
                #print(self.y)
                
                #print('y speed ' + str(self.yd))
                #print('x speed ' + str(self.xd))
                
                self.transitionTo(self.S2_Motors)
            
            elif(self.state == self.S2_Motors):
                
                if(self.contact == False):self
                    Tx = - self.Kx[1]*self.thetad_y - self.Kx[3]*.theta_y
                    Ty = - self.Ky[1]*self.thetad_x - self.Ky[3]*self.theta_x
                    
                else:
                    
                    #Tx =   +self.K1*self.xd*0 + self.K3*self.x*0
                    
                   # Ty =   -self.K1*self.yd - self.K3*self.y*0
                    
                    Tx = +self.Kx[0]*self.xd - self.Kx[1]*self.thetad_y + self.Kx[2]*self.x - self.Kx[3]*self.theta_y
                    
                    Ty = -self.Ky[0]*self.yd - self.Ky[1]*self.thetad_x - self.Ky[2]*self.y - self.Ky[3]*self.theta_x

                
                ## gain scheduling ###########################################################
                
                
                # if( self.thetad_x == 0 and abs(self.theta_x) > 0.03):
                #     Ty = -self.Ky[0]*self.yd - 2*self.Ky[1]*self.thetad_x - self.Ky[2]*self.y - 2*self.Ky[3]*self.theta_x
                    
                # if( self.thetad_y == 0 and abs(self.theta_y) > 0.03):
                #     Tx = +self.Kx[0]*self.xd - 2*self.Kx[1]*self.thetad_y + self.Kx[2]*self.x - 2*self.Kx[3]*self.theta_y
                    
                ###########################################################
                
                self.thetad_x == 0
                self.thetad_y == 0
                
                #K= R/(Vdc*kt)
                
                self.mot_x.set_duty(333.635265*Tx)
                self.mot_y.set_duty(333.635265*Ty)
                print('moty: ' + str(333.635265*Ty))
                print('motx: ' + str(333.635265*Tx))
                
                ## data data data
                if self.data == True:
                    self.arr_xd.append(self.xd)
                    self.arr_thd.append(self.thetad_y)
                    self.arr_x.append(self.x)
                    self.arr_thx.append(self.theta_y)
                    self.arr_Tx.append(Tx)
                    self.arr_contact(int(self.contact))
                    self.arr_time.append(self.curr_time/1000)
                
                
                self.transitionTo(self.S1_Values)
                
            else:
                pass
    
            self.next_time = utime.ticks_add(self.next_time, int(self.period*1e6))

    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState      
        
        
if __name__ == "__main__":
    
    import pyb
    from MotorDriver import MotorDriver
    from Encoder import Encoder
    from fault import nFAULT
    from touchDriver import touchDriver
    
    period = 0.01 #s

    # Setting up motors using MotorDriver class
    IN1 = pyb.Pin.cpu.B4
    IN2 = pyb.Pin.cpu.B5
    IN3 = pyb.Pin.cpu.B0
    IN4 = pyb.Pin.cpu.B1
    
    #nSLEEP = pyb.Pin.cpu.A15
    nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    nFAULT_pin = pyb.Pin.cpu.B2
    
    CH1 = 1
    CH2 = 2
    CH3 = 3
    CH4 = 4
    
    timer_3 = pyb.Timer(3, freq=20000)
    Mot2 = MotorDriver(nSLEEP,IN1,IN2,CH1,CH2,timer_3)
    Mot1 = MotorDriver(nSLEEP,IN3,IN4,CH3,CH4,timer_3)
    
    # Setting up enconders using Encoder class
    E1_CH1 = pyb.Pin.cpu.B6
    E1_CH2 = pyb.Pin.cpu.B7
    E2_CH1 = pyb.Pin.cpu.C6
    E2_CH2 = pyb.Pin.cpu.C7
    
    tim4 = pyb.Timer(4)
    tim8 = pyb.Timer(8)

    Enc1 = Encoder(period, tim4, E1_CH1, E1_CH2)
    Enc2 = Encoder(period, tim8, E2_CH1, E2_CH2)
    
    ym = pyb.Pin.cpu.A0
    xm = pyb.Pin.cpu.A1
    yp = pyb.Pin.cpu.A6
    xp = pyb.Pin.cpu.A7
    w  = .09936
    l  = .17664
    y0 = w/2
    x0 = l/2
    
    touch = touchDriver(ym, xm, yp, xp, w, l, x0, y0)
    #K = (speed,    angular velocity, pos, angle)    
    Kx = (-.6,  -.08,              -6, -2)
    Ky = (-.6,  -.08,             -6,  -2)
    
    # Cole's Gain: Kx = (-0.6,  -.1, -6, -2)
    # Cole's Gain: Ky = (-0.6,  -.1, -6,  -1.6)
    
    # Setting up fault tripping using the nFAULT class
    
    Mot2.enable()
    Mot1.enable()
    
    fault = nFAULT(nSLEEP)
    
    controller = platformController(period, Mot2_x, Mot1_y, Enc2, Enc1, fault, touch, Kx, Ky)
    
  
    
    while True: 
        try:
            controller.run()
        except KeyboardInterrupt:
            nSLEEP.low()
            print('Interupt Recognized')
            with open ("data.csv", "w") as file:
                for (a,b,c,d,e,f,t) in zip(controller.arr_xd ,controller.arr_thd, controller.arr_x, controller.arr_thx, controller.arr_Tx, controller.arr_contact, controller.arr_time):
                    file.write('{:}, {:}, {:}, {:}, {:}, {:}, {:}\r'.format(a,b, c, d, e, f, t))
            break
        
        
    
    
    
    
    
            