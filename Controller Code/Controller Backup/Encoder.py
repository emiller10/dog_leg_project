'''
@file Encoder.py

This file interacts with a physical encoder to measure the position of an 
geartrain ouput shaft.

It is intended to be used with a NUCLEO-L476RG board and US digital 1000 cpr encoder.
'''
import pyb 
import utime

class Encoder:
    '''
    @brief      A FSM used to measure an encoder position
    @details    This class implements a finite state machine to measure
                the output of a specific encoder wired to specific pins on
                the NUCLEO-L476RG board. 
    '''
    
    S0_INIT = 0
    
    S1_Run = 1
    
    def __init__ (self, interval, timer, CH1, CH2):
        '''
        Creates an encoder by initializing timers and pins.
        '''
        
        
        ## defining a timer to measure the encoder movement
        self.timer = timer
        
        ## defining a period to be used in the timer
        self.period = 0xFFFF
        
        ## initiallizing the timer, max period
        self.timer.init(prescaler = 0, period = self.period)
        
        ## Fedining channels 1 and 2 of the time, as well as timer mode
        self.timer.channel(1, pin=CH1, mode = pyb.Timer.ENC_AB)
        self.timer.channel(2, pin=CH2, mode = pyb.Timer.ENC_AB)
        
        
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## the position of the motor since initiaization, total revolutions included
        self.position = 0
        
        ## the last position of the motor
        self.last_position = 0
        
        self.interval = int(interval*1e6)
        
        #the timestamp since the first iteration
        self.start_time = utime.ticks_us() #the number of microseconds since hardware power on
        
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
        self.curr_time = utime.ticks_us()
        
        if (utime.ticks_diff(self.curr_time,self.next_time) >= 0):
            
            if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_Run)
                
                # print('encoder init')
            
            elif(self.state == self.S1_Run):
                self.transitionTo(self.S1_Run)
                #print('state 1: waiting 4 cmd')     
                self.update()
            
            else:
                pass
            
            self.next_time =  utime.ticks_add(self.next_time, self.interval)
               
    def update(self):
        '''
        @brief updates the position to the new location of the motor
        '''
        ## need to update this to work in radians?
        self.last_position = self.position
        
        delta = self.get_delta()
        
        self.position = self.position + delta
        
        #print('delta in ticks: ' + str(delta) + '     position: ' + str(self.position) + '     last position: ' + str(self.last_position))
        
        
        
    def get_position(self):
        '''
        @brief      returns the most recent position of the encoder
        '''
        return self.position
    
    
    def set_position(self, newposition):
        '''
        @brief      sets the position of the encoder to a new value defined by user
        '''
        # print('position set to newwwwwwwwwwwwwposition')
        # self.position = newposition
        # print(str(self.position))
        self.timer.counter(newposition)
        
        
    def get_delta(self):
        '''
        @brief      returns the delta between the last two positions in ticks
        '''
        
        # print('position: ' + str(self.position)) # -53
        # print('tim counter: ' + str(self.tim.counter())) # 65482
        # tim counter was couting wrong way??? encoder wired wrong??
        delta = self.timer.counter() - self.last_position 
        # print(delta) # 65535
        
        if(delta > ( (self.period/2) )):
            #print('delta is greater than period/2')
            # print(str(delta)) # 65535
            delta = delta - self.period
            # print('delta after correction' + str(delta)) # 0
            
        elif(delta < ( (-1*self.period/2) )):
            #print('delta is less than period/2')
            delta = delta + self.period
            
        else:
            #print('delta passed')
            pass
    
        delta  = delta
        
        return delta
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
            
            
        