'''
@file main.py

@brief This files shows how the MotorDriver, fault, and Encoder 
       classes can be used to drive motors on the ME 405 boards

@author: Cole and Ethan
'''
import pyb
from pyb import Timer, Pin, delay
from MotorDriver import MotorDriver
from fault import nFAULT
from Encoder import Encoder


# Setting up motors using MotorDriver class
IN1 = pyb.Pin.cpu.B4
IN2 = pyb.Pin.cpu.B5
IN3 = pyb.Pin.cpu.B0
IN4 = pyb.Pin.cpu.B1

nSLEEP = pyb.Pin.cpu.A15
nFAULT_pin = pyb.Pin.cpu.B2

CH1 = 1
CH2 = 2
CH3 = 3
CH4 = 4

timer_3 = Timer(3, freq=20000)
M1 = MotorDriver(nSLEEP,IN1,IN2,CH1,CH2,timer_3)
M2 = MotorDriver(nSLEEP,IN3,IN4,CH3,CH4,timer_3)

M1.enable()
M2.enable()

# Setting up enconders using Encoder class
E1_CH1 = pyb.Pin.cpu.B6
E1_CH2 = pyb.Pin.cpu.B7
E2_CH1 = pyb.Pin.cpu.C6
E2_CH2 = pyb.Pin.cpu.C6

tim4 = pyb.Timer(4)
tim8 = pyb.Timer(8)

interval = (0.2)

Enc1 = Encoder(interval, tim4, E1_CH1, E1_CH2)
Enc2 = Encoder(interval, tim8, E2_CH1, E2_CH2)


EN = Pin(Pin.cpu.A15, mode=Pin.OUT_PP, value=1)

# Setting up fault tripping using the nFAULT class
fault = nFAULT(nSLEEP)

M1.enable()
M2.enable()

while True:

    try:
        
        print('Testing Motor 1')
        M1.set_duty(80)
        delay(500)
        M1.set_duty(0)
        delay(500)
        M1.set_duty(-80)
        delay(500)
        M1.set_duty(0)
        delay(500)
        
        print('Testing Motor 2')
        M2.set_duty(50)
        delay(500)
        M2.set_duty(0)
        delay(500)
        M2.set_duty(-50)
        delay(500)
        M2.set_duty(0)
        delay(500)
        
    except:
        M1.disable()
        M2.disable()
        print('See ya')
        break
    
   
