
'''
@file touchDriver.py

@brief This file creates a touchdriver object to interface with the ER-TP080-1 resistive touch panel

'''

import pyb


class touchDriver:
    
    def __init__ (self, ym, xm, yp, xp, w, l, x0, y0):
        ''' 
        @brief initializes the touchDriver

        @param ym A pyb.Pin object for the y negative pin.
        @param xm A pyb.Pin object for the x negative pin.
        @param yp A pyb.Pin object for the y positive pin.
        @param xp A pyb.Pin object for the x positive pin.
        @param w The width of the available resistive area.
        @param l The length of the available resistive area.
        @param x0 The x origin of the resistive area
        @param y0 The y origing of the resistive area

        '''
        
        self.ym = ym
        self.xm = xm
        self.yp = yp
        self.xp = xp
        
        #defining length, width, and center coords, might change center later to be w/2 and l/2
        self.width = w
        self.length = l
        self.x0 = x0
        self.y0 = y0
        
        
    def xScan(self):
        ''' 
        @brief This method scans the x component by configuring self.xp to a push pull output set high
                 and self.xm to a push pull output set low. The voltage is then measures by floating
                 yp and measuring the voltage at ym.
        @param ymadc ADC from pyb class that measures voltage of the ym pin. 
        @return Double representing x position of touch input
        '''
        
        # X IS LENGTH = 176
        
        self.xp.init(mode = pyb.Pin.OUT_PP)
        self.xp.high()
        self.xm.init(mode = pyb.Pin.OUT_PP)
        self.xm.low()
        self.yp.init(mode = pyb.Pin.IN)
        ymadc = pyb.ADC(self.ym)

        #testing a delay value
        pyb.udelay(4)
        return self.x0 - self.length*ymadc.read()/4095
        
    def yScan(self):
        ''' 
        @brief This method scans the y component by configuring self.yp to a push pull output set high
                 and self.ym to a push pull output set low. The voltage is then measures by floating
                 xp and measuring the voltage at xm.
        @param xmadc ADC from pyb class that measures voltage of the xm pin. 
        @return double representing y position of touch input
        '''
        
        self.yp.init(mode = pyb.Pin.OUT_PP)
        self.yp.high()
        self.ym.init(mode = pyb.Pin.OUT_PP)
        self.ym.low()
        self.xp.init(mode = pyb.Pin.IN)
        xmadc = pyb.ADC(self.xm)
        

        #testing a delay value
        pyb.udelay(4)
        
        return self.y0 -self.width*xmadc.read()/4095
        
    def zScan(self):
        '''
        @brief This method scans the z component by configuring self.yp to a push pull output set high
                 and self.xm to a push pull output set low. The voltage is then measuring either the ym or xp pins.
                 The ym pin will be high if there is no contact on the board, but lower than high if there is contact.
        @param ymadc ADC from pyb class that measures voltage of the ym pin. 
        @param xpadc ADC from pyb class that measures voltage of the xp pin. 
        
        @return Boolean: False if ym pin is high, else True
        '''
        
        self.yp.init(mode = pyb.Pin.OUT_PP)
        self.yp.high()
        self.xm.init(mode = pyb.Pin.OUT_PP)
        self.xm.low()
        ymadc = pyb.ADC(self.ym)
        xpadc = pyb.ADC(self.xp)
        
        pyb.udelay(4)
        
        if ymadc.read() < 4000:   
            return  True
        else:
            return False
        
    def xyzScan(self):
        '''
        @brief Function that will call all of the position functions
        @return List of x and y positions and z boolean for if contact is made.
        '''
        return((self.xScan(), self.yScan(), self.zScan()))
        
if __name__ == "__main__":
    import utime
    
    ym = pyb.Pin.cpu.A0
    xm = pyb.Pin.cpu.A1
    yp = pyb.Pin.cpu.A6
    xp = pyb.Pin.cpu.A7
    w  = 99.36
    l  = 176.64
    y0 = w/2
    x0 = l/2
    
    touch = touchDriver(ym, xm, yp, xp, w, l, x0, y0)
    
    startTime = utime.ticks_us()
    
    while True:
        print(touch.xyzScan())
        pyb.delay(1000)
    
    # for n in range(100):
    #     touch.xyzScan()

    # totalTime = utime.ticks_diff( utime.ticks_us(), startTime)
    # avgTime = totalTime/100
    # print(avgTime)
        
    
    