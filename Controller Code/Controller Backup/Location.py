import pyb
import utime

class Location():
    
    def __init__(self, y_m, x_m, y_p, x_p, length, width):
        self.y_m = y_m
        self.x_m = x_m
        self.y_p = y_p
        self.x_p = x_p
        
        self.length = length
        self.width = width
        
    def xScan(self):
        pyb.Pin(self.x_p, mode = pyb.Pin.OUT_PP)
        pyb.Pin(self.x_m, mode = pyb.Pin.OUT_PP)
        pyb.Pin(self.y_p, mode = pyb.Pin.IN)
        pyb.Pin(self.y_m, mode = pyb.Pin.IN)
        self.x_p.high()
        self.x_m.low()
        adc = pyb.ADC(self.y_m)
        return self.length*((adc.read()/4095)-0.5) 
            
    def yScan(self):
        pyb.Pin(self.y_p, mode = pyb.Pin.OUT_PP)
        pyb.Pin(self.y_m, mode = pyb.Pin.OUT_PP)
        pyb.Pin(self.x_p, mode = pyb.Pin.IN)
        pyb.Pin(self.x_m, mode = pyb.Pin.IN)
        self.y_p.high()
        self.y_m.low()
        adc = pyb.ADC(self.x_m)
        return self.length*((adc.read()/4095)-0.5)
        
    def zScan(self):
        pyb.Pin(self.y_p, mode = pyb.Pin.OUT_PP)
        pyb.Pin(self.x_m, mode = pyb.Pin.OUT_PP)
        pyb.Pin(self.x_p, mode = pyb.Pin.IN)
        pyb.Pin(self.y_m, mode = pyb.Pin.IN)
        self.y_p.high()
        self.x_m.low()
        adc = pyb.ADC(self.y_m)
        z = adc.read()
        if z < 4000:
            return True
        else:
            return False
        
    def xyzScan(self):
        return (self.xScan(),self.yScan(),self.zScan())
        
if __name__ == '__main__':
    length = 0
    width = 0

    y_m = pyb.Pin.cpu.A0
    x_m = pyb.Pin.cpu.A1
    y_p = pyb.Pin.cpu.A6
    x_p = pyb.Pin.cpu.A7


    while True:
        x = Location(y_m, x_m, y_p, x_p,length = 177,width = 99)
        start = utime.ticks_us()
        x.xyzScan()
        end = utime.ticks_us()
        print(utime.ticks_diff(end,start))
