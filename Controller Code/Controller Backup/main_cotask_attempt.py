# -*- coding: utf-8 -*-
'''
@file main.py
Created on Mon Mar 15 12:57:48 2021

@author: Cole
'''

import pyb
import gc
from micropython import alloc_emergency_exception_buf
import Encoder
import fault
import touchDriver
import MotorDriver

# create emergency buffer to store errors that are thrown inside ISR
alloc_emergency_exception_buf(200)

def controller_X(motor):
    '''@briedf Controller for a single motor in X (length) direction
    '''
    
    ''' What do we need to do in here?
    
    get y dot
    get theta dot x
    get y
    get theta x
    mutliple by gains to get torque
    convert torque to pwm
    profit?
    '''
    
    
    
def controller_Y():
    '''@briedf Controller for a single motor in Y (width) direction
    '''
    
    ''' What do we need to do in here?
    
    get x dot
    get theta dot y
    get x
    get theta y
    mutliple by gains to get torque
    convert torque to pwm
    profit?
    '''