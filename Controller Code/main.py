import pyb
import utime
import array
import math


class Dog_Leg_Controller:
    '''
    @class Dog_Leg_Controller.py Controller for Mechanical Dog Leg
    @brief This controller class drives two individual motors connected through a four-member systom for a mechanical dog leg.
    @details This system takes inputs in the form of x and y positions and uses an equation from a MATLAB script modelling a four-member
             mechanical dog leg.
    @author Ethan Miller
    @date June 7, 2021
    '''
    
    ## Init state, also used to level the platform
    S0_INIT = 0
    ## State 1: updating positions and speed
    S1_Values = 1
    ## State 2: motor control
    S2_Motors = 2
    
    def __init__(self,motor_1,motor_2,enc_1,enc_2):
        '''
        @brief Setting up class object and saving variables
        @param motor_1 Motor object from MotorDriver class designated to be motor 1 from the schematic
        @param motor_2 Motor object from MotorDriver class designated to be motor 2 from the schematic
        @param enc_1 Encoder object from Encoder class initialized to encoder attached to motor_1 
        @param enc_2 Encoder object from Encoder class initialized to encoder attached to motor_2 
        @param period delta time between each reading and updating of motor position
        @param fault Fault object from fault class that turns motors off if overpowering happens
        @param K_th1 Gain values for PD controller for theta_1
        @param K_th2 Gain values for PD controller for theta_2
        @param theta_1 Current theta value for motor 1
        @param thetad_1 Current angular speed value for motor 1
        @param theta_2 Current theta value for motor 2
        @param thetad_2 Current angular speed value for motor 2
        '''
        
        ## defining motors and encoders
        self.motor_1 = motor_1
        self.motor_2 = motor_2
        self.enc_1 = enc_1
        self.enc_2 = enc_2
        
        ## defining fault object for overpowering fault
        # self.fault = fault
        
        # ## defining gain values
        # self.K_th1 = K_th1
        # self.K_th2 = K_th2
        
        ## init positions and speed for motor x
        self.theta_1 = 0
        self.thetad_1 = 0
        
        self.theta_2 = 0
        self.thetad_2 = 0
        
        #the timestamp since the first iteration
        self.start_time = utime.ticks_us() #the number of microseconds since hardware power on
        
        self.period = .05 #update time in Hz
        
        self.next_time = utime.ticks_add(self.start_time, int(self.period*1e6))
        self.state = 0
        
        ########################################
        
    def pathTrack(self):
        '''
        @brief function that creates the x and y positions for 
        '''
        ## init path positions for system to follow
        self.total_time = 10 #seconds
        self.w = 2*math.pi/self.total_time
        
        # Center and radii of the ellipse path
        self.r_1 = 2.5
        self.r_2 = .75
        self.x_c = 2.75
        self.y_c = -7.5
        
        # Create lists for loops below
        self.time = []
        self.x = []
        self.y = []
        
        ### For loop that creates the parameterized x and y with regards to time. 
        for p in range(0,self.total_time*(1/self.period)):
            self.time.append((self.period)*p)
            
            # Ellipse path in x and y parameterized by time
            self.x.append(self.r_1*math.cos(self.w*self.time[p]) + self.x_c);
            self.y.append(self.r_2*math.sin(self.w*self.time[p]) + self.y_c);   
        
        # print(self.time)
        # print(self.x)
        # print(self.y)
        
        
    def run(self):
        '''
        @brief 
        '''
        # Run pathTrack() to get the paths for the curve we want to run.
        self.pathTrack()
        
        self.curr_time = utime.ticks_us()
        
        if(utime.ticks_diff(self.curr_time, self.next_time) >= 0):
        
            if(self.state == self.S0_INIT):
            
                self.balance = pyb.ExtInt(pyb.Pin.cpu.C13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, self.balance_callback)
                self.balance.enable()
                self.balanced = False   #This will be the flag for whether the button has been pressed
                
                ##
                print('Please balance about the Y-axis. Tap the blue button when done.')
                while self.balanced == False:
                    pass
                self.enc_1.set_position(int(math.pi/0.001571))
                self.balanced = False
                print('motor 1 zeroed')

                utime.sleep(1)
                print('Please balance about the Y-axis. Tap the blue button when done.')
                while self.balanced == False:
                    pass
                self.enc_2.set_position(int(2*math.pi/0.001571))
                self.balanced = False
                print('motor 2 zeroed')
                
                self.motor_1.enable()
                self.motor_2.enable()
                print('Motors Enabled')
                # self.fault.enable_fault()
                # print('Fault detection enabled')
                
                self.last_time = utime.ticks_us()
                self.current_time = 0
                self.i = 0
                
                temp_error = [0,0]
                self.error_th1 = 0
                self.error_th2 = 0
                
                 # Coefficients from the curve fit equations from MATLAB script
                self.th1_coeff = [18.51598837,-3.321273709, 7.923820656, 0.457258547, -1.284208589, 1.571004879, -0.028294029, 0.103273418, -0.15692962, 0.136797497, 0.000774326, -0.0028906, 0.006012131, -0.006284806, 0.004501654]
                self.th2_coeff = [-0.879132658, -0.387979407, -3.641984738, -0.12725338, -0.425637084, -0.920617237, -0.011442375, -0.054829355, -0.092541188, -0.105193892, -0.00076928, -0.002907341, -0.00597405, -0.006352852, -0.004605693]
                
                for j in range(0,len(self.th1_coeff)-1):
                    # print(j)
                    self.th1_coeff[j] = float(self.th1_coeff[j])
                    self.th2_coeff[j] = float(self.th2_coeff[j])
                self.balance.disable()
                self.transitionTo(self.S1_Values)
                
                
            elif(self.state == self.S1_Values):
                
                if self.i == len(self.x)-1:
                    self.i = 0
                
                ##################################################################
        
                # Need to get the next position in terms of x and y
                self.x_next = float(self.x[self.i])
                self.y_next = float(self.y[self.i])
                # print(self.x_next)
                # print(self.y_next)
                
                #### Now lets convert it from the x and y to theta_1 and theta_2 for the motor controls
                self.theta_1_next = self.th1_coeff[0] + self.th1_coeff[1]*self.x_next + self.th1_coeff[2]*self.y_next + self.th1_coeff[3]*self.x_next**2 + self.th1_coeff[4]*self.x_next*self.y_next + self.th1_coeff[5]*self.y_next**2 +  self.th1_coeff[6]*self.x_next**3 + self.th1_coeff[7]*self.x_next**2*self.y_next + self.th1_coeff[8]*self.x_next*self.y_next**2 + self.th1_coeff[9]*self.y_next**3 + self.th1_coeff[10]*self.x_next**4 + self.th1_coeff[11]*self.x_next**3*self.y_next + self.th1_coeff[12]*self.x_next**2*self.y_next**2 + self.th1_coeff[13]*self.x_next*self.y_next**3 + self.th1_coeff[14]*self.y_next**4
                print('next theta1 ' + str(self.theta_1_next))
                
                               
                self.theta_2_next = self.th2_coeff[0] + self.th2_coeff[1]*self.x_next + self.th2_coeff[2]*self.y_next + self.th2_coeff[3]*self.x_next**2 + self.th2_coeff[4]*self.x_next*self.y_next + self.th2_coeff[5]*self.y_next**2 +  self.th2_coeff[6]*self.x_next**3 + self.th2_coeff[7]*self.x_next**2*self.y_next + self.th2_coeff[8]*self.x_next*self.y_next**2 + self.th2_coeff[9]*self.y_next**3 + self.th2_coeff[10]*self.x_next**4 + self.th2_coeff[11]*self.x_next**3*self.y_next + self.th2_coeff[12]*self.x_next**2*self.y_next**2 + self.th2_coeff[13]*self.x_next*self.y_next**3 + self.th2_coeff[14]*self.y_next**4
                print('next theta2 ' + str(self.theta_2_next))
                ##################################################################
                
                #### Now we need to find the current angle of the motor using the encoder. Remember that the encoder needs to be calibrated.
                #### We also need to find the time between the last encoder update and this encoder update to 
                
                # we have to update the encoders first, then we can get the value
                self.enc_1.update()
                self.theta_1 = self.enc_1.get_position()*0.001571
                print('theta 1: ' + str(self.theta_1))
                
                self.enc_2.update()
                self.theta_2 = self.enc_2.get_position()*0.001571
                print('theta 2: ' + str(self.theta_2))
                
                ## Find the amount of time between the last encoder check and this one, this will be the delta time
                self.current_time = utime.ticks_us()
                self.error_time_delta = utime.ticks_diff(self.current_time,self.last_time)
                self.last_time = self.current_time
                
                ##################################################################
                
                #### We want to find the error between the current and next angle to determine the power distributed to the motors:
                
                ## find the error, and then the rate of change of the error
                
                ## store the last error in memory so we can recall it to find the rate of change
                temp_error = [self.error_th1, self.error_th2]
                
                self.error_th1 = self.theta_1 - self.theta_1_next
                self.error_th2 = self.theta_2 - self.theta_2_next
                print('error 1: ' + str(self.error_th1))
                print('error 2: ' + str(self.error_th2))
                
                self.thetad_1 = (self.error_th1 - temp_error[0])/(self.error_time_delta/10**6)
                self.thetad_2 = (self.error_th2 - temp_error[1])/(self.error_time_delta/10**6)
                
                print('delta error 1: ' + str(self.thetad_1))
                print('delta error 2: ' + str(self.thetad_2))

                self.transitionTo(self.S2_Motors)
            
            elif(self.state == self.S2_Motors):
                
                ##################################################################
            
                #### Now we have the errors and the deltas, so we need to find the duty cycle (aka power) to distribute to the motors
                #### This is a simple PD controller so lets set up the K values and see how this goes.
                
                K1_P = 100
                K1_D = 15
                K2_P = 100
                K2_D = 15
                
                mot_1_duty = K1_P*self.error_th1 + K1_D*self.thetad_1
                mot_2_duty = K2_P*self.error_th2 + K2_D*self.thetad_2
                
                print(mot_1_duty)
                print(mot_2_duty)
                print('/n')
                
                self.motor_1.set_duty(mot_1_duty)
                self.motor_2.set_duty(mot_2_duty)
                self.i = self.i+1
                self.transitionTo(self.S1_Values)
                
            else:
                pass
    
            self.next_time = utime.ticks_add(self.next_time, int(self.period*1e6))

    def transitionTo(self, newState):
        '''
        @brief Updates the variable defining the next state to run
        '''
        self.state = newState
        
    def balance_callback(self,pin):
        if self.balanced == False:
            self.balanced = True
            print('Balanced!!')
            
        else:
            pass
        
        
        
            
        
        
        # # This while loop with run one full period of the curve.
        # while i in range(0,self.period*self.total_time):
        
            
            
            # i = i+1
            
        
if __name__ == "__main__":
    import pyb
    from MotorDriver import MotorDriver
    from Encoder import Encoder
    from fault import nFAULT
    from touchDriver import touchDriver
    
    period = 0.01 #s

    # Setting up motors using MotorDriver class
    IN1 = pyb.Pin.cpu.B4
    IN2 = pyb.Pin.cpu.B5
    IN3 = pyb.Pin.cpu.B0
    IN4 = pyb.Pin.cpu.B1
    
    #nSLEEP = pyb.Pin.cpu.A15
    nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    nFAULT_pin = pyb.Pin.cpu.B2
    
    CH1 = 1
    CH2 = 2
    CH3 = 3
    CH4 = 4
    
    timer_3 = pyb.Timer(3, freq=20000)
    Mot2 = MotorDriver(nSLEEP,IN1,IN2,CH1,CH2,timer_3)
    Mot1 = MotorDriver(nSLEEP,IN3,IN4,CH3,CH4,timer_3)
    
    # Setting up enconders using Encoder class
    E1_CH1 = pyb.Pin.cpu.B6
    E1_CH2 = pyb.Pin.cpu.B7
    E2_CH1 = pyb.Pin.cpu.C6
    E2_CH2 = pyb.Pin.cpu.C7
    
    tim4 = pyb.Timer(4)
    tim8 = pyb.Timer(8)

    Enc2 = Encoder(period, tim4, E1_CH1, E1_CH2)
    Enc1 = Encoder(period, tim8, E2_CH1, E2_CH2)
    
    fault = nFAULT(nSLEEP)
    
    controller = Dog_Leg_Controller(Mot1, Mot2, Enc1, Enc2)
    
    while True: 
        try:
            controller.run()
        except KeyboardInterrupt:
            nSLEEP.low()
            print('Interupt Recognized')
            break
        
        
        
        
        
        
        
        
        
        
        
        
        
         