'''
@file MotorDriver.py

@brief This file contains the class attributes for a generic motor driver interacting with a DRV8847 on a Nucleo.

@author: Cole and Ethan
'''
import pyb



class MotorDriver:
    '''
    @brief      Class for generic Motor Controller that interacts with DRV8847 
                motor driver connected to the Nucleo. 
    @details    By sending a pulse width modulation signal to the input pins to 
                the motor controller on the NUCLEO-L476RG board, this program
                can change the power output to the motors, therefore changing
                the speed.
    
    '''
    
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, CH1, CH2, timer):
        '''
        @brief Initialization of motor driver with variables
        
        @param nSLEEP_pin Pin that can be set to high or low voltage in order to enable and disable motors
        @param IN1_pin Input to half bridge 1
        @param IN2_pin Input to half bridge 2
        @param CH1 Timer channel 1 being used by this motor driver
        @param CH2 Timer channel 2 being used by this motor driver
        @param timer Timer used by motor driver that is running in PWM mode, used for PWM generation on IN1_pin and IN2_pin
        '''
        
        # save variables to object:
        self.nSLEEP_pin = nSLEEP_pin
        self.nSLEEP_pin.low()
        
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        
        
        # other variables:
        self.duty = 0
        self.status = 'OFF'
        self.fault = False
        
        # setting up the motor:
        self.motor_CH1 = timer.channel(CH1, mode = pyb.Timer.PWM, pin = self.IN1_pin)
        self.motor_CH2 = timer.channel(CH2, mode = pyb.Timer.PWM, pin = self.IN2_pin)
        
        # Setting the motors to OFF on initializaiton
        self.motor_CH1.pulse_width_percent(0)
        self.motor_CH2.pulse_width_percent(0)
        
        
    def enable(self):
        '''
        Enables Motor: set the nSLEEP pin to HIGH
        '''
        self.nSLEEP_pin.high()
        pyb.delay(100)
        self.status = 'ON'
        self.motor_CH1.pulse_width_percent(0)
        self.motor_CH2.pulse_width_percent(0)
    
    def disable(self):
        '''
        Disable Motor: set the nSLEEP pin to LOW
        '''
        self.nSLEEP_pin.low()
        self.status = 'OFF'
        self.set_duty(0)
        
    def motor_status(self):
        '''
        Returns the motor status (enabled [ON] or disabled [OFF]). 
        Mostly for debugging purposes
        '''
        print(self.status)
    
    def set_duty(self,duty):
        '''
        Set duty cycle to given value
        '''
        self.duty = duty
        if abs(self.duty) > 40:
            self.duty = 40*(self.duty/abs(self.duty))
            print(str(self.duty)+'\n\n\n\n\n\n')
        if self.duty > 0:
            self.motor_CH1.pulse_width_percent(self.duty)
            self.motor_CH2.pulse_width_percent(0)
        elif self.duty < 0:
            self.motor_CH1.pulse_width_percent(0)
            self.motor_CH2.pulse_width_percent(-1*(self.duty))
        else:
            self.motor_CH1.pulse_width_percent(0)
            self.motor_CH2.pulse_width_percent(0)
            
if __name__ == "__main__":
    
        # Setting up motors using MotorDriver class
    IN1 = pyb.Pin.cpu.B4
    IN2 = pyb.Pin.cpu.B5
    IN3 = pyb.Pin.cpu.B0
    IN4 = pyb.Pin.cpu.B1
    
    nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    nFAULT_pin = pyb.Pin.cpu.B2
    
    CH1 = 1
    CH2 = 2
    CH3 = 3
    CH4 = 4
    
    timer_3 = pyb.Timer(3, freq=20000)
    Mot1_y = MotorDriver(nSLEEP,IN1,IN2,CH1,CH2,timer_3)
    Mot2_x = MotorDriver(nSLEEP,IN3,IN4,CH3,CH4,timer_3)
    
    Mot1_y.enable()
    Mot2_x.enable()
    Mot1_y.set_duty(100)